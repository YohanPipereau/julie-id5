import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';
import babel from '@rollup/plugin-babel';

export default {
  input: 'dist/index.js',
  output: {
    file: 'bundle.js',
    format: 'iife',
  },
  plugins: [ resolve(), json(), babel({ babelHelpers: 'bundled' }) ]
};
